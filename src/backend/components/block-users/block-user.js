/**
 * Model representing a blocked user.
 *
 * @author FiveFuenf
 */
class BlockUser extends AbstractModel {
    /**
     * Initializes the model.
     *
     * @param {string} name Name of the user to block
     */
    constructor(name) {
        super();

        this._name = name;
    }


    serialize() {
        return {
            'name': this._name
        };
    }


    static deserialize(serialized) {
        return new BlockUser(serialized.name);
    }


    /**
     * Gets the name of the blocked user.
     *
     * @returns {string}
     */
    get name() {
        return this._name;
    }


    /**
     * Sets the name of the user to block.
     *
     * @param {string} name Username
     *
     * @returns {void}
     */
    set name(name) {
        this._name = name;
    }
}
