/**
 * View for exporting user settings.
 *
 * @author FiveFuenf
 */
class ExportSettingsView extends AbstractView {
    constructor(container) {
        super(container);

        // Contains the main button triggering the export action
        this.exportButton = this._container.querySelector('button');

        // Container for status messages
        this.statusContainer = document.querySelector('#settingsStatus');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        this.exportButton.addEventListener('click', () => {
            let settings = JSON.stringify(Settings.getInstance().settings);
            GeneralUtility.download(settings, 'settings-' + DateUtility.timestamp + '.json', 'application/json', downloadId => {
                this.statusContainer.append(GeneralUtility.createAlert('success', 'Settings downloaded.', 5000));
            });
        });
    }
}
