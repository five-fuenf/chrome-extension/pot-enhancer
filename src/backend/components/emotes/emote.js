/**
 * Model representing an emote.
 *
 * @author FiveFuenf
 */
class Emote extends AbstractModel {
    /**
     * Initializes the model.
     *
     * @param {string} identifier Unique identifier of the emote
     * @param {string} url URL pointing to the image/icon of the emote
     */
    constructor(identifier, url) {
        super();

        this._identifier = this.prepareIdentifier(identifier);
        this._url = url;
    }


    serialize() {
        return {
            'identifier': this._identifier,
            'url': this._url
        };
    }


    static deserialize(serialized) {
        return new Emote(serialized.identifier, serialized.url);
    }


    /**
     * Gets the unique identifier.
     *
     * @returns {string}
     */
    get identifier() {
        return this._identifier;
    }


    /**
     * Sets the unique identifier.
     *
     * @param {string} identifier Identifier to set
     *
     * @returns {void}
     */
    set identifier(identifier) {
        this._identifier = this.prepareIdentifier(identifier);
    }


    /**
     * Prepares and validates the identifier to fulfill certain criteria.
     *
     * - Minimum three characters
     * - Only letters and numbers
     *
     * @param {string} identifier Identifier to validate
     */
    prepareIdentifier(identifier) {
        if (identifier === undefined) {
            return '000';
        }

        identifier = identifier.replace(/[^\w\d!?]/g, '');

        if (identifier.length < 3) {
            for (let i = 0; i <= (3 - identifier.length); i++) {
                identifier += '0';
            }
        }

        return identifier;
    }


    /**
     * Gets the URL.
     *
     * @returns {string}
     */
    get url() {
        return this._url;
    }


    /**
     * Sets the URL.
     *
     * @param {string} url URL to set
     *
     * @returns {void}
     */
    set url(url) {
        this._url = url;
    }
}
