/**
 * View for resetting user settings.
 *
 * @author FiveFuenf
 */
class ResetSettingsView extends AbstractView {
    constructor(container) {
        super(container);

        // Button initiating the reset
        this.resetButton = this._container.querySelector('button');

        // Checkbox for confirming the reset
        this.confirmCheckbox = this._container.querySelector('#confirmReset');

        // Container for status messages
        this.statusContainer = document.querySelector('#settingsStatus');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        // Uncheck to confirm-checkbox by default
        this.confirmCheckbox.checked = false;

        this.resetButton.addEventListener('click', () => {
            if (!this.confirmCheckbox.checked) {
                this.statusContainer.append(
                    GeneralUtility.createAlert('primary', 'Check the checkbox below the reset-button to make sure, that really all settings should be reset.', 10000)
                );
            } else {
                Settings.getInstance().reset();
            }
        });
    }
}
