/**
 * Route for the redirect after creating a thread.
 *
 * @author FiveFuenf
 * @deprecated Not really used anymore, as redirects to previous pages are made directly on the post/thread create pages via JavaScript
 */
class CreateThreadRedirectRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+newthread.php$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < CreateThreadRedirectRoute.matchers.length; i++) {
            if (CreateThreadRedirectRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Immediately redirect
        DomUtility.body.classList.add('d-none');
        let redirectLink = document.querySelector('a.notice');
        window.location.href = redirectLink.getAttribute('href');
    }
}
