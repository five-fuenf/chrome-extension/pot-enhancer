/**
 * Contains functions for dealing with browser cookies.
 *
 * @author FiveFuenf
 */
class CookieUtility {
    constructor() {
    }


    /**
     * Gets a specific cookie.
     *
     * @param {string} key Key name of the cookie to get
     *
     * @returns {string}
     */
    static get(key) {
        let cookies = CookieUtility.getAll();
        if (cookies.hasOwnProperty(key)) {
            return cookies[key];
        }
        return '';
    }


    /**
     * Gets all cookies.
     *
     * @returns {JSON}
     */
    static getAll() {
        let rawCookies = document.cookie.split(';');
        let cookies = {};

        for (let i = 0; i < rawCookies.length; i++) {
            let parts = rawCookies[i].split('=');
            let key = parts[0];
            let value = parts[1];

            cookies[key] = value;
        }

        return cookies;
    }


    /**
     * Sets a cookie.
     *
     * @param {string} key Key of the cookie to set
     * @param {string} value Value for the cookie
     * @param {Date} [expires] Date when the cookie expires (defaults to nothing)
     * @param {string} [path] Path of the cookie (defaults to "/")
     *
     * @returns {void}
     */
    static set(key, value, expires, path) {
        let cookie = key + '=' + value;

        if (expires !== undefined) {
            cookie += ';' + expires.toUTCString();
        }

        if (path !== undefined) {
            cookie += ';path=' + path;
        }

        document.cookie(cookie);
    }
}
