// Load app settings
try {
    Settings.getInstance().load(function (settings) {
        DebugUtility.setIsEnabled(Settings.getInstance().get('functions.debug'));

        DebugUtility.log('Settings loaded.');
        DebugUtility.log(Settings.getInstance().settings);
        DebugUtility.log('Initializing router.');

        // Inject CSS code right at the start
        if (Settings.getInstance().get('injectCssCode')) {
            let style = document.createElement('style');
            style.textContent = Settings.getInstance().get('injectCssCode');
            document.head.append(style);
        }

        // Add layout class early
        if (Settings.getInstance().get('functions.responsiveLayout')) {
            document.body.classList.add('responsive-layout');
        }

        let router = new Router();
    });
} catch (e) {
    throw new Error('Loading settings failed: ' + e);
}
