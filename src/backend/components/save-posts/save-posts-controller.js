/**
 * Controls save posts options.
 *
 * @author FiveFuenf
 */
class SavePostsController extends AbstractController {
    constructor(view) {
        super(view);

        this.view.on('remove', (e) => this.remove(e));
    }


    /**
     * Removes a saved post.
     *
     * @param {int} pid PID of the post to remove
     *
     * @returns {void}
     */
    remove(pid) {
        let path = ['savePosts', pid];
        Settings.getInstance().remove(path);
    }
}
