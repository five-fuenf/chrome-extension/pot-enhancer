/**
 * View for adjusting emotes.
 *
 * @author FiveFuenf
 */
class EmotesView extends AbstractView {
    constructor(container) {
        super(container);

        // Define possible events for observers
        this.observers = {
            'save': [],
            'remove': []
        };

        // Button for creating new emotes
        this.createEmoteButton = this._container.querySelector('#createEmote');

        // List all configured emotes
        this.emotesList = this._container.querySelector('#emoteList');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        this.createEmoteButton.addEventListener('click', () => {
            let emoteRow = this.createEmoteRow(true);
            this.emotesList.prepend(emoteRow);
        });

        let emotes = Settings.getInstance().get('emotes');
        Object.keys(emotes).forEach((identifier) => {
            let emoteRow = this.createEmoteRow(false, emotes[identifier]);
            this.emotesList.prepend(emoteRow);
        });
    }


    /**
     * Creates a new row defining an emote.
     *
     * @param {boolean} isNew Whether the form is created new and empty, not bound to an event yet
     * @param {Emote} [emote] Existing emote to create row for
     *
     * @returns {Element}
     */
    createEmoteRow(isNew, emote) {
        let inputGroup = document.createElement('div');
        inputGroup.classList.add('input-group');

        let inputGroupPrepend = document.createElement('div');
        inputGroupPrepend.classList.add('input-group-text', 'p-0');

        // Controls
        let saveButton = document.createElement('button');
        saveButton.classList.add('btn', 'btn-primary');
        saveButton.innerHTML = 'Save';
        saveButton.setAttribute('type', 'button');

        let removeButton = document.createElement('button');
        removeButton.classList.add('btn', 'btn-danger');
        removeButton.innerHTML = 'Remove';
        removeButton.setAttribute('type', 'button');

        // Define emote
        let identifierInput = document.createElement('input');
        identifierInput.classList.add('form-control');
        identifierInput.setAttribute('type', 'text');
        identifierInput.setAttribute('placeholder', 'Shortcut to use in post textarea');

        let urlInput = document.createElement('input');
        urlInput.classList.add('form-control');
        urlInput.setAttribute('type', 'text');
        urlInput.setAttribute('placeholder', 'URL pointing to the emote');

        // Preview of emote image
        let inputGroupAppend = document.createElement('div');
        inputGroupAppend.classList.add('input-group-text', 'p-0', 'd-none');

        let previewImageContainer = document.createElement('div');
        previewImageContainer.classList.add('preview-image', 'input-group-text');

        let previewImage = document.createElement('img');

        inputGroupPrepend.append(saveButton);
        previewImageContainer.append(previewImage);
        inputGroupAppend.append(previewImageContainer, removeButton);
        inputGroup.append(inputGroupPrepend, identifierInput, urlInput, inputGroupAppend);

        // Set existing emote values
        let previousIdentifier;
        if (emote !== undefined) {
            previousIdentifier = emote.identifier;
            identifierInput.value = emote.identifier;
            urlInput.value = emote.url;
            inputGroupAppend.classList.remove('d-none');
            previewImage.setAttribute('src', emote.url);
        }

        // Setup events
        saveButton.addEventListener('click', () => {
            let identifier = identifierInput.value;
            let url = urlInput.value;
            if (!identifier || !url) {
                this.container.append(GeneralUtility.createAlert('warning', 'Fill out identifier and URL.', 5000));
                return;
            }

            try {
                this.trigger('save', {
                    'identifier': identifier,
                    'url': url,
                    'isNew': isNew,
                    'previousIdentifier': previousIdentifier
                });
            } catch (e) {
                this.container.append(GeneralUtility.createAlert('warning', 'There already exists an emote with this identifier.', 5000));
                return;
            }

            isNew = false;
            previousIdentifier = identifier;

            // Update preview image
            inputGroupAppend.classList.remove('d-none');
            previewImage.setAttribute('src', url);
        });

        removeButton.addEventListener('click', () => {
            let identifier = identifierInput.value;
            if (!identifier) {
                inputGroup.remove();
                return;
            }

            this.trigger('remove', identifier);
            inputGroup.remove();
        });

        return inputGroup;
    }
}
