/**
 * View for adjusting the general list of posts.
 *
 * @author FiveFuenf
 */
class Posts extends AbstractView {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this.initializeDom();
    }


    /**
     * @override
     */
    render() {
        super.render();
    }


    /**
     * @override
     */
    initializeDom() {
        super.initializeDom();

        // Add classes for easier style adjustments
        // Container of all posts
        let postsContainer = DomUtility.postsContainer;
        postsContainer.classList.add('posts');

        // Posts itself
        let posts = DomUtility.posts;
        GeneralUtility.addClasses(posts, 'post');

        let postContents = DomUtility.postContents;
        GeneralUtility.addClasses(postContents, 'content');

        // Content elements of a post
        let postTables = DomUtility.postTables;
        GeneralUtility.addClasses(postTables, 'table');

        let postCodeBlocks = DomUtility.postCodeBlocks;
        GeneralUtility.addClasses(postCodeBlocks, 'code-block');

        // Allow to bookmark a post without the alert boxes
        this.initializeBookmark();

        // Allow to save/bookmark posts
        this.initializeSavePosts();

        // Mark post from hash in URL
        this.initializeMarkPost();

        // Allow to resize embedded videos
        this.initializeVideoResize();

        // Fixes external Imgur links
        this.fixImgurLinks();
    }


    /**
     * Hooks into the regular bookmark-creation process.
     *
     * @returns {void}
     */
    initializeBookmark() {
        let createBookmarkLinks = DomUtility.postsContainer.querySelectorAll('a[href^="javascript:setBookmark"]');
        createBookmarkLinks.forEach(link => {
            // Command is executed by a script on the site, something like: javascript:setBookmark(id, token)
            let command = link.getAttribute('href');

            // Create a custom link element to work around existing click listeners
            let customLink = document.createElement('a');
            customLink.setAttribute('href', 'javascript:;');
            customLink.classList.add('create-bookmark');
            customLink.innerHTML = 'Lesezeichen';

            customLink.addEventListener('click', e => {
                e.preventDefault();

                customLink.classList.add('active');

                let commandParts = RegExUtility.find(command, new RegExp(/javascript:setBookmark\((\d+), '([a-zA-Z0-9]+)'/, 'gi'));
                if (commandParts.length === 2) {
                    fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/async/set-bookmark.php?PID=' + commandParts[0] + '&token=' + commandParts[1], {
                        'method': 'GET',
                    })
                        .then(() => {
                            customLink.classList.remove('active');
                            customLink.classList.add('used');
                        })
                        .catch(error => {
                            throw new Error('Could not create bookmark: ' + error);
                        });
                } else {
                    customLink.classList.remove('active');
                    customLink.classList.add('disabled');

                    throw new Error('Could not create bookmark.');
                }
            });

            GeneralUtility.insertAfter(customLink, link);
            link.remove();
        });


        let removeBookmarkLinks = DomUtility.postsContainer.querySelectorAll('a[onclick^="removeBookmark"]');
        removeBookmarkLinks.forEach(link => {
            // Command is executed by a script on the site, something like: removeBookmark(id, token) && deleteElem($('delpbm-'))
            let command = link.getAttribute('onclick');
            link.onclick = false;

            // Create a custom link element to work around existing click listeners
            let customLink = document.createElement('a');
            customLink.setAttribute('href', 'javascript:;');
            customLink.classList.add('delete-bookmark');
            customLink.innerHTML = 'Lesezeichen';

            customLink.addEventListener('click', e => {
                e.preventDefault();

                customLink.classList.add('active');

                let commandParts = RegExUtility.find(command, new RegExp(/removeBookmark\((\d+), '([a-zA-Z0-9]+)'/, 'gi'));
                if (commandParts.length === 2) {
                    fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/async/remove-bookmark.php?BMID=' + commandParts[0] + '&token=' + commandParts[1], {
                        'method': 'GET',
                    })
                        .then(() => {
                            customLink.classList.remove('active');
                            customLink.classList.add('disabled');
                        })
                        .catch(error => {
                            throw new Error('Could not delete bookmark: ' + error);
                        });
                } else {
                    customLink.classList.remove('active');
                    customLink.classList.add('disabled');

                    throw new Error('Could not delete bookmark.');
                }
            });

            GeneralUtility.insertAfter(customLink, link);
            link.remove();
        });
    }


    /**
     * Initializes the save/bookmark posts functionality.
     *
     * @returns {void}
     */
    initializeSavePosts(posts) {
        DomUtility.posts.forEach(post => {
            let postFooter = post.nextElementSibling;

            // Retrieve PID from the link to this post
            let postLink = postFooter.querySelector('a.postlink');
            let postUrl = postLink.getAttribute('href');
            let postUrlHashParts = postUrl.split('#');
            if (postUrlHashParts.length !== 2 || !postUrlHashParts[1]) {
                throw new Error('PID cannot be determined.');
            }

            let postId = parseInt(postUrlHashParts[1].replace('reply_', ''));
            let threadId = GeneralUtility.getRequestParameter('TID');
            if (isNaN(postId) || !threadId) {
                throw new Error('TID or PID is invalid.');
            }

            // Insert save-post link
            let reportPostLink = postFooter.querySelector('a[href^="reportpost.php"]');

            // Add post to settings (existing keys=PIDs will be overwritten)
            let savePostLink = document.createElement('a');
            savePostLink.setAttribute('href', '#reply_' + postId);
            savePostLink.innerHTML = 'Save post';

            let savePostLinkSpacer = document.createElement('b');
            savePostLinkSpacer.innerHTML = '&nbsp;|&nbsp;';

            GeneralUtility.insertBefore(savePostLink, reportPostLink);
            GeneralUtility.insertAfter(savePostLinkSpacer, savePostLink);

            let hasBeenSaved = false;
            savePostLink.addEventListener('click', () => {
                if (hasBeenSaved) {
                    return;
                }

                // Ask user for optional description
                let description = prompt('Enter a description if needed', '');
                if (description === undefined || !description) {
                    description = null;
                } else {
                    description = description.trim();
                }

                // Create new saved post
                let savePosts = Settings.getInstance().get('savePosts');
                let savePost = new SavePost(
                    postId,
                    threadId,
                    description,
                    DateUtility.timestamp
                );

                savePosts[postId] = savePost.serialize();
                Settings.getInstance().set('savePosts', savePosts);

                savePostLink.innerHTML = 'Saved';
                savePostLink.removeAttribute('href');
                hasBeenSaved = true;
            });
        });
    }


    /**
     * Initializes the function to mark a post when a post-hash in the URL is given.
     * As this extension adjusts the layout and CSS, the site might change its dimensions upon loading. This can lead to anchor-tags not being hit correctly
     * by hashes in the URL. Therefore marking a post manually emphasizes the selected post from the hash in the URL.
     *
     * @returns {void}
     */
    initializeMarkPost() {
        let postId = GeneralUtility.getPostIdFromHash();
        if (!postId) {
            return;
        }

        let target = DomUtility.postsContainer.querySelector('a[name="reply_' + postId + '"]');
        if (!target) {
            return;
        }
        let targetPost = target.closest('.post');

        // Do nothing for the last post
        if (targetPost === DomUtility.posts[DomUtility.posts.length - 1]) {
            return;
        }

        targetPost.classList.add('hash-target');
    }


    /**
     * Initializes the function to resize embedded videos in posts.
     *
     * @returns {void}
     */
    initializeVideoResize() {
        let videos = DomUtility.postsContainer.querySelectorAll('video');
        let iFrames = DomUtility.postsContainer.querySelectorAll('iframe');

        // Enables resizing of the given element
        let setupResizer = function (video) {
            // Rearrange containers
            let container = document.createElement('div');
            container.classList.add('video-container');

            let handle = document.createElement('div');
            handle.classList.add('handle');

            let arrow = document.createElement('div');
            arrow.setAttribute('draggable', true);
            arrow.classList.add('arrow', 'down');

            let source = video.parentElement.querySelector('.source');

            GeneralUtility.insertAfter(container, video);
            handle.append(source, arrow);
            container.append(video, handle);

            // Resize video when holding the arrow
            // Only one coordinate is checked, as the video will resize accordingly in the other dimension
            let dragStartX, dragEndX, deltaX = 0;

            arrow.addEventListener('dragstart', e => {
                dragStartX = e.screenX;
            });

            arrow.addEventListener('dragend', e => {
                dragEndX = e.screenX;
                deltaX = dragEndX - dragStartX;

                let originalHeight = video.offsetHeight;
                let height = originalHeight + deltaX;
                let heightRatio = height / originalHeight;

                // Calculate width as a ratio to the new height
                let width = video.offsetWidth * heightRatio;

                if (video.getAttribute('height')) {
                    video.setAttribute('height', height)
                    video.setAttribute('width', width)
                } else {
                    video.style.height = height + 'px';
                    video.style.width = width + 'px';
                }
            });
        };

        if (videos) {
            videos.forEach(video => {
                setupResizer(video);
            });
        }

        if (iFrames) {
            iFrames.forEach(iFrame => {
                setupResizer(iFrame);
            });
        }
    }


    /**
     * Fixes any external Imgur links.
     *
     * @returns {void}
     */
    fixImgurLinks() {
        let imgurLinks = DomUtility.postsContainer.querySelectorAll('a[href*="imgur"]');
        imgurLinks.forEach(link => {
            let rel = link.getAttribute('rel');
            if (!rel) {
                link.setAttribute('rel', 'noreferrer');
            } else {
                link.setAttribute('rel', 'noreferrer ' + rel);
            }
        });
    }
}
