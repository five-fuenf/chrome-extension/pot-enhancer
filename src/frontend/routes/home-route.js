/**
 * Route for the home page.
 *
 * @author FiveFuenf
 */
class HomeRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+index\.php$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < HomeRoute.matchers.length; i++) {
            if (HomeRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Adjust navbar
        let navbar = new Navbar();

        // Adjust bookmarks
        let bookmarks = new Bookmarks();
    }
}
