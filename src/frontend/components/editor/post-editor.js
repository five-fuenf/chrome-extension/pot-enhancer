/**
 * Editor for creating or editing a post.
 *
 * @author FiveFuenf
 */
class PostEditor extends AbstractEditor {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this._container.classList.add('post');

        this.render();
        this.initializeDom();
    }


    /**
     * @override
     */
    render() {
        super.render();

        // Get any existing values
        let originalFormContainer = DomUtility.postFormContainer;
        let originalForm = originalFormContainer.querySelector('form');

        // Grab any existing hidden form values from the original document
        let threadId = '', postId = '', token = '';
        let threadIdInput = document.querySelector('input[name="TID"]');
        let postIdInput = document.querySelector('input[name="PID"]');
        let tokenInput = document.querySelector('input[name="token"]');

        // Create new post
        if (Router.activeRoute instanceof CreatePostRoute) {
            // When quoting a post, the textarea might have some value already
            let message = originalFormContainer.querySelector('textarea[name="message"]');
            this._message.value = GeneralUtility.decodeHtmlEntities(message.value);
        }

        // Edit post
        if (Router.activeRoute instanceof EditPostRoute) {
            // Grab existing values of the post and set them in the custom editor
            let title = originalFormContainer.querySelector('input[name="edit_title"]');
            this._title.value = title.value;

            let message = originalFormContainer.querySelector('textarea[name="message"]');
            this._message.value = GeneralUtility.decodeHtmlEntities(message.value);

            let icon = originalFormContainer.querySelector('input[name="edit_icon"]:checked');
            let selectedIcon = this._container.querySelector('.icons input[value="' + icon.value + '"]');
            if (selectedIcon) {
                selectedIcon.checked = true;
            }

            // The original checkbox sent by the server will always be checked; still add this check here, maybe this behaviour will changed
            let enableConvertUrls = originalFormContainer.querySelector('input[name="edit_converturls"]:checked');
            if (!enableConvertUrls) {
                this._enableConvertUrls.checked = false;
            }

            let disableBbcode = originalFormContainer.querySelector('input[name="edit_disablebbcode"]:checked');
            if (disableBbcode) {
                this._disableBbcode.checked = true;
            }

            let disableSmilies = originalFormContainer.querySelector('input[name="edit_disablesmilies"]:checked');
            if (disableSmilies) {
                this._disableSmilies.checked = true;
            }
        }

        // Create or edit post
        if (Router.activeRoute instanceof CreatePostRoute || Router.activeRoute instanceof EditPostRoute) {
            // The existing hidden values should hold all values needed to perform a succesful request to the server
            if (threadIdInput) {
                threadId = threadIdInput.value;
            }
            if (postIdInput) {
                postId = postIdInput.value;
            }
            if (tokenInput) {
                token = tokenInput.value;
            }
        }

        // Quick-post in thread route
        if (Router.activeRoute instanceof ThreadRoute) {
            // Add message to quote last post, if needed
            this.initializeQuoteLastPost();

            // For quick-posting there are no existing hidden values.
            // ThreadId can be easily retrieved from the URL
            // PostId is not needed
            // Fortuinately token is availabe as a JavaScript variable on the site
            threadId = GeneralUtility.getRequestParameter('TID');

            // TokenID cannot be access directly via wrappedJSObject like in Firefox addon
            // Sucky workaround below, but thats chrome-extensions for 'ya
            for (let i = 0; i < window.document.scripts.length; i++) {
                let script = window.document.scripts[i].innerText;
                let matches = RegExUtility.find(script, new RegExp('var token_newreply = \'([a-zA-Z0-9]+)\';', 'g'));
                if (matches.length > 0) {
                    token = matches[0];
                    break;
                }
            }

            if (!token) {
                throw new Error('Could not get token.');
            }
        }


        // Add hidden values to the form
        if (!threadId || !token) {
            throw new Error('Could not determine threadId or token.');
        }

        // Focus textarea to start writing quickly
        // Don' for quick-reply, otherwise browser will scroll down
        if (Router.activeRoute instanceof CreatePostRoute || Router.activeRoute instanceof EditPostRoute) {
            this._message.focus();
        }


        // Setup post function
        let postDebounce = false;
        this._submitButton.addEventListener('click', e => {
            if (postDebounce || !this.validate()) {
                return;
            }
            postDebounce = true;

            this.disable();
            let selectedIcon = this._container.querySelector('input[name="icon"]:checked');

            // Create post or quick-post
            let promise;
            if (Router.activeRoute instanceof CreatePostRoute || Router.activeRoute instanceof ThreadRoute) {
                DebugUtility.log('Sending post creation request.');

                promise = this.sendPostRequest({
                    'TID': threadId,
                    'token': token,
                    'message': this._message.value,
                    'post_title': this._title.value,
                    'post_icon': selectedIcon.value ? selectedIcon.value : '', // Note: null-coalesce is detected as error by Firefox Addon upload process...
                    'post_converturls': this._enableConvertUrls.checked ? '1' : '0',
                    'post_disablebbcode': this._disableBbcode.checked ? '1' : '0',
                    'post_disablesmilies': this._disableSmilies.checked ? '1' : '0'
                });
            }

            // Edit a post
            else if (Router.activeRoute instanceof EditPostRoute) {
                DebugUtility.log('Sending post editing request.');

                promise = this.sendEditRequest({
                    'TID': threadId,
                    'PID': postId,
                    'token': token,
                    'message': this._message.value,
                    'edit_title': this._title.value,
                    'edit_icon': selectedIcon.value ? selectedIcon.value : '', // Note: null-coalesce is detected as error by Firefox Addon upload process...
                    'edit_converturls': this._enableConvertUrls.checked ? '1' : '0',
                    'edit_disablebbcode': this._disableBbcode.checked ? '1' : '0',
                    'edit_disablesmilies': this._disableSmilies.checked ? '1' : '0'
                });
            } else {
                throw new Error('Invalid route for posting.');
            }

            promise.then(response => {
                DebugUtility.log('Request finished, response ready.');
                return response.text();
            })
                .then(text => {
                    DebugUtility.log('Response text ready.');

                    // Forum might answer with spam protection
                    if (text.indexOf('Du postest zu viel in zu kurzer Zeit') !== -1) {
                        DebugUtility.log('Spam protection in effect.');

                        this.addLogMessage('warning', 'Du postest zu viel in zu kurzer Zeit! Bitte warte einen Moment.');
                        this.enable();
                        return false;
                    }

                    return true;
                })
                .then(success => {
                    if (!success) {
                        DebugUtility.log('Request failed, do not redirect.');
                        return;
                    }

                    // If creating a post, redirect to the last post in the corresponding thread
                    if (Router.activeRoute instanceof CreatePostRoute || Router.activeRoute instanceof ThreadRoute) {
                        DebugUtility.log('Request successful, redirecting to: ' + GeneralUtility.buildUrlToThread(threadId, -1));
                        GeneralUtility.redirectToThread(threadId, -1);
                    }

                    // If editing a post, redirect to the thread at the edited post
                    else if (Router.activeRoute instanceof EditPostRoute) {
                        DebugUtility.log('Request successful, redirecting to: ' + GeneralUtility.buildUrlToPost(postId, threadId));
                        GeneralUtility.redirectToPost(postId, threadId);
                    }

                    this.enable();
                });

            promise.catch(error => {
                throw new Error('Could not create new post: ' + error);
            });

            promise.finally(() => {
                setTimeout(() => {
                    postDebounce = false;
                }, 3000);
            });
        });


        // Setup preview function
        let previewDebounce = false;
        this._previewButton.addEventListener('click', e => {
            if (previewDebounce || !this.validate()) {
                return;
            }
            previewDebounce = true;

            this.disable();
            this._previewContainer.classList.add('pulse');
            this._previewContainer.append(GeneralUtility.createSpinner('md', true));

            let selectedIcon = this._container.querySelector('input[name="icon"]:checked');

            // Create post or quick-post
            let promise;
            if (Router.activeRoute instanceof CreatePostRoute || Router.activeRoute instanceof ThreadRoute) {
                promise = this.sendPreviewRequestForCreating({
                    'TID': threadId,
                    'token': token,
                    'message': this._message.value,
                    'post_title': this._title.value,
                    'post_icon': selectedIcon.value ? selectedIcon.value : '', // Note: null-coalesce is detected as error by Firefox Addon upload process...
                    'post_converturls': this._enableConvertUrls.checked ? '1' : '0',
                    'post_disablebbcode': this._disableBbcode.checked ? '1' : '0',
                    'post_disablesmilies': this._disableSmilies.checked ? '1' : '0'
                });
            }

            // Edit a post
            else if (Router.activeRoute instanceof EditPostRoute) {
                promise = this.sendPreviewRequestForEditing({
                    'TID': threadId,
                    'PID': postId,
                    'token': token,
                    'message': this._message.value,
                    'edit_title': this._title.value,
                    'edit_icon': selectedIcon.value ? selectedIcon.value : '', // Note: null-coalesce is detected as error by Firefox Addon upload process...
                    'edit_converturls': this._enableConvertUrls.checked ? '1' : '0',
                    'edit_disablebbcode': this._disableBbcode.checked ? '1' : '0',
                    'edit_disablesmilies': this._disableSmilies.checked ? '1' : '0'
                });
            } else {
                throw new Error('Invalid route for preview.');
            }

            // Copy preview container into current view
            promise.then(response => {
                let textPromise = response.text();
                textPromise.then(text => {
                    let responseDocument = GeneralUtility.parseTemplateString(text);
                    let contentContainer = GeneralUtility.findChildren(responseDocument, 'div[align="center"]')[1];

                    if (!contentContainer) {
                        throw new Error('Content container not found.');
                    }

                    let previewContainer = contentContainer.querySelector('table')
                        .querySelectorAll('tbody > tr > td > table > tbody > tr')[2]
                        .querySelector('td');

                    this.loadPreview(previewContainer.innerHTML, this._title.value, selectedIcon.value);
                });

                textPromise.catch(error => {
                    throw new Error('Could not get text from response: ' + error);
                });

                this.enable();
            });

            promise.catch(error => {
                throw new Error('Could not fetch preview page: ' + error);
            });

            promise.finally(() => {
                setTimeout(() => {
                    previewDebounce = false;
                }, 3000);
            });
        });


        // Listen for keyboard shortcuts
        document.addEventListener('keydown', e => {
            // Trigger preview
            if (
                (e.ctrlKey && e.shiftKey && e.which === 13) || // CTRL + SHIFT + ENTER
                (e.altKey && e.which === 86) // ALT + V
            ) {
                DebugUtility.log('Trigger preview.');
                this._previewButton.dispatchEvent(new Event('click'));
                return;
            }

            // Trigger save
            if (
                (e.ctrlKey && e.which === 13) || // CTRL + ENTER
                (e.altKey && e.shiftKey && e.which === 13) || // ALT + SHIFT + ENTER
                (e.altKey && e.which === 83) // ALT + S
            ) {
                DebugUtility.log('Trigger submit.');
                this._submitButton.dispatchEvent(new Event('click'));
                return;
            }
        });
    }


    /**
     * @override
     */
    initializeDom() {
        super.initializeDom();
    }


    /**
     * @override
     */
    validate() {
        this.clearLogMessages();
        let valid = true;

        if (GeneralUtility.isStringEmpty(this._message.value)) {
            valid = false;
        }

        if (!valid) {
            this.addLogMessage('warning', 'Nachricht muss ausgefüllt sein!');
        }

        return valid;
    }


    /**
     * Adds a functions to the editor to allow for quoting the last post. Useful when e.g. the post would be on a new site.
     *
     * @returns {void}
     */
    initializeQuoteLastPost() {
        // Check if new post would be on a new page
        if (!DomUtility.posts || DomUtility.posts.length !== 30) {
            return;
        }

        // Get quote-URL from last post
        var lastPost = DomUtility.posts[29];
        var lastPostFunctions = lastPost.nextElementSibling;
        var quoteLink = lastPostFunctions.querySelector('tr.color1 table table a[href^="newreply.php?PID="]');
        var quoteUrl = quoteLink.getAttribute('href');

        let quoteLastPostTemplate = `
<div>
    <div class="alert alert-info">
        Dieser Post wird auf einer neuen Seite sein. Soll stattdessen der letzte Post zitiert werden?
        <a id="acceptQuoteLastPost" class="link-styled dark">Ja (Achtung: Seite lädt neu)!</a> -
        <a id="dismissQuoteLastPost" class="link-styled dark">Lass mich in Ruhe!</a>
    </div>
</div>
`;

        let parsedQuoteLastPostTemplate = GeneralUtility.parseTemplateString(quoteLastPostTemplate);
        GeneralUtility.insertBefore(parsedQuoteLastPostTemplate, this._container.querySelector('.form-group'));

        // Accept clicked; redirect to quote-post page
        var acceptButton = parsedQuoteLastPostTemplate.querySelector('#acceptQuoteLastPost');
        acceptButton.addEventListener('click', () => {
            window.location.href = quoteUrl;
        });

        // Dismiss clicked; hide info window
        var dismissButton = parsedQuoteLastPostTemplate.querySelector('#dismissQuoteLastPost');
        dismissButton.addEventListener('click', () => {
            parsedQuoteLastPostTemplate.classList.add('d-none');
        });
    }


    /**
     * Performs the request for getting the post preview when creating a post/thread.
     *
     * @param {JSON} post Post values
     * @param {int} post.TID Thread ID
     * @param {string} post.token Token
     * @param {string} post.message Message
     * @param {string} [post.post_title] New title
     * @param {int} [post.post_icon] New icon
     * @param {int} [post.post_converturls] Enable [URL] conversion
     * @param {int} [post.post_disablebbcode] Disable BB-Code
     * @param {int} [post.post_disablesmilies] Disable smilies
     *
     * @returns {Promise}
     * @throws When request parameters are invalid
     */
    sendPreviewRequestForCreating(post) {
        if (!post.hasOwnProperty('TID') ||
            !post.hasOwnProperty('token') ||
            !post.hasOwnProperty('message') ||
            post['TID'] <= 0) {
            throw new Error('Invalid request parameters.');
        }

        post['message'] = this.parseFinalMessage(post['message']);

        // Add default values
        post['preview'] = 'Preview';

        return fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/newreply.php', {
            'method': 'POST',
            'body': GeneralUtility.createFormData(post)
        });
    }


    /**
     * Performs the request for creating a post.
     *
     * @param {JSON} post Post values
     * @param {int} post.TID Thread ID
     * @param {string} post.token Token
     * @param {string} post.message Message
     * @param {string} [post.post_title] Disable BB-Code
     * @param {int} [post.post_icon] Disable smilies
     * @param {int} [post.post_converturls] Enable [URL] conversion
     * @param {int} [post.post_disablebbcode] Disable BB-Code
     * @param {int} [post.post_disablesmilies] Disable smilies
     *
     * @returns {Promise}
     * @throws When request parameters are invalid
     */
    sendPostRequest(post) {
        if (!post.hasOwnProperty('TID') ||
            !post.hasOwnProperty('token') ||
            !post.hasOwnProperty('message') ||
            post['TID'] <= 0) {
            throw new Error('Invalid request parameters.');
        }

        post['message'] = this.parseFinalMessage(post['message']);

        // Add default values
        post['submit'] = 'Eintragen';

        return fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/newreply.php', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            'body': GeneralUtility.createQueryString(post)
        });
    }


    /**
     * Performs the request for getting the post preview when editing a post.
     *
     * @param {JSON} post Post values
     * @param {int} post.TID Thread ID
     * @param {string} post.token Token
     * @param {string} post.message Message
     * @param {int} post.PID Existing post ID
     * @param {string} [post.edit_title] New title
     * @param {int} [post.edit_icon] New icon
     * @param {int} [post.edit_converturls] Enable [URL] conversion
     * @param {int} [post.edit_disablebbcode] Disable BB-Code
     * @param {int} [post.edit_disablesmilies] Disable smilies
     *
     * @returns {Promise}
     * @throws When request parameters are invalid
     */
    sendPreviewRequestForEditing(post) {
        if (!post.hasOwnProperty('TID') ||
            !post.hasOwnProperty('PID') ||
            !post.hasOwnProperty('token') ||
            !post.hasOwnProperty('message') ||
            post['TID'] <= 0) {
            throw new Error('Invalid request parameters.');
        }

        post['message'] = this.parseFinalMessage(post['message']);

        // Add default values
        post['preview'] = 'Preview';

        return fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/editreply.php', {
            'method': 'POST',
            'body': GeneralUtility.createFormData(post)
        });
    }


    /**
     * Performs the request for editing a post.
     *
     * @param {JSON} post Post values
     * @param {int} post.TID Thread ID
     * @param {int} post.PID Post ID to edit
     * @param {string} post.token Token
     * @param {string} post.message Message
     * @param {string} [post.edit_title] New title
     * @param {int} [post.edit_icon] New icon
     * @param {int} [post.edit_converturls] Enable [URL] conversion
     * @param {int} [post.edit_disablebbcode] Disable BB-Code
     * @param {int} [post.edit_disablesmilies] Disable smilies
     *
     * @returns {Promise}
     * @throws When request parameters are invalid
     */
    sendEditRequest(post) {
        if (!post.hasOwnProperty('TID') ||
            !post.hasOwnProperty('PID') ||
            !post.hasOwnProperty('token') ||
            !post.hasOwnProperty('message') ||
            post['TID'] <= 0 ||
            post['PID'] <= 0) {
            throw new Error('Invalid request parameters.');
        }

        post['message'] = this.parseFinalMessage(post['message']);

        // Add default values
        post['submit'] = 'Eintragen';

        return fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/editreply.php', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            'body': GeneralUtility.createQueryString(post)
        });
    }
}
