/**
 * Controls injected CSS options.
 *
 * @author FiveFuenf
 */
class InjectCssController extends AbstractController {
    constructor(view) {
        super(view);

        this.view.on('save', (e) => this.save(e));
    }


    /**
     * Saves the CSS code to inject.
     *
     * @param {*} event Event parameters
     *
     * @returns {void}
     * @throws When the identifier already exists
     */
    save(event) {
        let code = '';

        if (event.code !== undefined && event.code) {
            code = event.code;
        }

        Settings.getInstance().set('injectCssCode', event.code);
    }
}
