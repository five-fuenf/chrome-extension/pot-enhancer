/**
 * Model representing a saved post.
 *
 * @author FiveFuenf
 */
class SavePost extends AbstractModel {
    /**
     * Initializes the model.
     *
     * @param {string} postId postId of the post
     * @param {string} threadId TID of the post
     * @param {string} description Description
     * @param {int} timestamp Timestamp when the post was saved
     */
    constructor(postId, threadId, description, timestamp) {
        super();

        this._postId = postId;
        this._threadId = threadId;
        this._description = description;
        this._timestamp = timestamp;
    }


    serialize() {
        return {
            'postId': this._postId,
            'threadId': this._threadId,
            'description': this._description,
            'timestamp': this._timestamp
        };
    }


    static deserialize(serialized) {
        return new SavePost(serialized.postId, serialized.threadId, serialized.description, serialized.timestamp);
    }


    /**
     * Gets the postId to the post.
     *
     * @returns {string}
     */
    get postId() {
        return this._postId;
    }


    /**
     * Sets the postId to the post.
     *
     * @param {string} postId postId
     *
     * @returns {void}
     */
    set postId(postId) {
        this._postId = postId;
    }


    /**
     * Gets the TID to the post.
     *
     * @returns {string}
     */
    get threadId() {
        return this._threadId;
    }


    /**
     * Sets the TID to the post.
     *
     * @param {string} threadId TID
     *
     * @returns {void}
     */
    set threadId(threadId) {
        this._threadId = threadId;
    }


    /**
     * Gets the post description.
     *
     * @returns {string|null}
     */
    get description() {
        return this._description;
    }


    /**
     * Sets the post description.
     *
     * @param {string} description TID
     *
     * @returns {void}
     */
    set description(description) {
        this._description = description;
    }


    /**
     * Gets the timestamp when the post was saved.
     *
     * @returns {int}
     */
    get timestamp() {
        return this._timestamp;
    }


    /**
     * Sets the timestamp when the post was saved.
     *
     * @param {int} timestamp Timestamp
     *
     * @returns {void}
     */
    set timestamp(timestamp) {
        this._timestamp = timestamp;
    }
}
