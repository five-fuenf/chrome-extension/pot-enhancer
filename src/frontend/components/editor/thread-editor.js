/**
 * Editor used for creating a new thread.
 *
 * @author FiveFuenf
 */
class ThreadEditor extends AbstractEditor {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this._container.classList.add('thread');

        // Important DOM objects - will be declared in render()-method
        this._subtitle = null;
        this._tags = null;

        this.render();
        this.initializeDom();
    }


    /**
     * @override
     */
    render() {
        super.render();

        // Extend base template
        let subtitleTemplate = `
<div class="form-group row">
    <label for="thread_subtitle" class="col-md-2 col-form-label text-left">Untertitel</label>
    <div class="col-md-10">
        <input type="text" class="form-control" id="thread_subtitle" name="thread_subtitle" tabindex="2" />
    </div>
</div>
`;
        let parsedSubtitleTemplate = GeneralUtility.parseTemplateString(subtitleTemplate);
        GeneralUtility.insertAfter(parsedSubtitleTemplate, this._title.parentElement.parentElement);

        let tagsTemplate = `
<div class="form-group row my-2">
    <label for="thread_tags" class="col-md-2 col-form-label text-left">Tags</label>
    <div class="col-md-10">
        <input type="text" class="form-control" id="thread_tags" name="thread_tags" placeholder="foo,bar,baz" tabindex="3" />
    </div>
</div>
`;
        let parsedTagsTemplate = GeneralUtility.parseTemplateString(tagsTemplate);
        GeneralUtility.insertAfter(parsedTagsTemplate, parsedSubtitleTemplate);

        this._subtitle = parsedSubtitleTemplate.querySelector('input[name="thread_subtitle"]');
        this._tags = parsedTagsTemplate.querySelector('input[name="thread_tags"]');

        // Get any existing values
        let originalForm = DomUtility.postFormContainer;

        // Thread start post is handled by regular PostEditor
        let title = originalForm.querySelector('input[name="thread_title"]');
        let subtitle = originalForm.querySelector('input[name="thread_subtitle"]');
        let tags = originalForm.querySelector('input[name="thread_tags"]');
        let message = originalForm.querySelector('textarea[name="message"]');
        let icon = originalForm.querySelector('input[name="thread_icon"]:checked');


        // Add hidden form values to the new form
        let boardIdInput = document.querySelector('input[name="BID"]');
        let tokenInput = document.querySelector('input[name="token"]');

        let boardId = '', token = '';
        if (boardIdInput) {
            boardId = boardIdInput.value;
        }
        if (tokenInput) {
            token = tokenInput.value;
        }

        if (!boardId || !token) {
            throw new Error('Could not determine boardId or token.');
        }

        // Focus title to start writing quickly
        this._title.focus();


        // Setup post function
        let postDebounce = false;
        this._submitButton.addEventListener('click', e => {
            if (postDebounce || !this.validate()) {
                return;
            }
            postDebounce = true;

            this.disable();
            let selectedIcon = this._container.querySelector('input[name="icon"]:checked');

            let promise = this.sendPostRequest({
                'BID': boardId,
                'token': token,
                'thread_title': this._title.value,
                'thread_subtitle': this._subtitle.value,
                'thread_icon': selectedIcon.value ? selectedIcon.value : '', // Note: null-coalesce is detected as error by Firefox Addon upload process...
                'message': this._message.value,
                'thread_tags': this._tags.value,
                'thread_converturls': this._enableConvertUrls.checked ? '1' : '0',
                'thread_disablebbcode': this._disableBbcode.checked ? '1' : '0',
                'thread_disablesmilies': this._disableSmilies.checked ? '1' : '0'
            });

            promise.then(response => {
                GeneralUtility.redirectToBoard(boardId);
                this.enable();
            });

            promise.catch(error => {
                throw new Error('Could not create new thread: ' + error);
            });

            promise.finally(() => {
                setTimeout(() => {
                    postDebounce = false;
                }, 3000);
            });
        });


        // Setup preview function
        let previewDebounce = false;
        this._previewButton.addEventListener('click', e => {
            if (previewDebounce || !this.validate()) {
                return;
            }
            previewDebounce = true;

            this.disable();
            this._previewContainer.classList.add('pulse');
            this._previewContainer.append(GeneralUtility.createSpinner('md', true));

            let selectedIcon = this._container.querySelector('input[name="icon"]:checked');

            let promise = this.sendPreviewRequest({
                'BID': boardId,
                'token': token,
                'thread_title': this._title.value,
                'thread_subtitle': this._subtitle.value,
                'thread_icon': selectedIcon.value ? selectedIcon.value : '', // Note: null-coalesce is detected as error by Firefox Addon upload process...
                'message': this._message.value,
                'thread_tags': this._tags.value,
                'thread_converturls': this._enableConvertUrls.checked ? '1' : '0',
                'thread_disablebbcode': this._disableBbcode.checked ? '1' : '0',
                'thread_disablesmilies': this._disableSmilies.checked ? '1' : '0'
            });

            promise.then(response => {
                let textPromise = response.text();
                textPromise.then(text => {
                    let responseDocument = GeneralUtility.parseTemplateString(text);
                    let contentContainer = GeneralUtility.findChildren(responseDocument, 'div[align="center"]')[1];

                    if (!contentContainer) {
                        throw new Error('Content container not found.');
                    }

                    let previewContainer = contentContainer.querySelector('table')
                        .querySelectorAll('tbody > tr > td > table > tbody > tr')[0]
                        .querySelectorAll('td > table > tbody > tr')[4]
                        .querySelector('td');

                    this.loadPreview(previewContainer.innerHTML, this._title.value, selectedIcon.value);
                });

                textPromise.catch(error => {
                    throw new Error('Could not get text from response: ' + error);
                });

                this.enable();
            });

            promise.catch(error => {
                throw new Error('Could not fetch preview page: ' + error);
            });

            promise.finally(() => {
                setTimeout(() => {
                    previewDebounce = false;
                }, 3000);
            });
        });


        // Listen for keyboard shortcuts
        document.addEventListener('keydown', e => {
            // Trigger preview
            if (
                (e.ctrlKey && e.shiftKey && e.which === 13) || // CTRL + SHIFT + ENTER
                (e.altKey && e.which === 86) // ALT + V
            ) {
                DebugUtility.log('Trigger preview.');
                this._previewButton.dispatchEvent(new Event('click'));
                return;
            }

            // Trigger save
            if (
                (e.ctrlKey && e.which === 13) || // CTRL + ENTER
                (e.altKey && e.shiftKey && e.which === 13) || // ALT + SHIFT + ENTER
                (e.altKey && e.which === 83) // ALT + S
            ) {
                DebugUtility.log('Trigger submit.');
                this._submitButton.dispatchEvent(new Event('click'));
                return;
            }
        });
    }


    /**
     * @override
     */
    initializeDom() {
        super.initializeDom();
    }


    /**
     * @override
     */
    validate() {
        this.clearLogMessages();
        let valid = true;

        if (GeneralUtility.isStringEmpty(this._title.value)) {
            valid = false;
        }
        if (GeneralUtility.isStringEmpty(this._message.value)) {
            valid = false;
        }

        if (!valid) {
            this.addLogMessage('warning', 'Titel und Nachricht müssen ausgefüllt sein!');
        }

        return valid;
    }


    /**
     * Performs the request for getting the thread preview.
     *
     * @param {JSON} post Post values
     * @param {int} post.BID Board
     * @param {string} post.token Token
     * @param {string} post.thread_title Title
     * @param {string} post.message Initial post message
     * @param {string} [post.thread_subtitle] Subtitle
     * @param {int} [post.thread_icon] Icon identifier
     * @param {string} [post.thread_tags] Comma separated tags
     * @param {int} [post.thread_converturls] Enable [URL] conversion
     * @param {int} [post.thread_disablebbcode] Disable BB-Code
     * @param {int} [post.thread_disablesmilies] Disable smilies
     *
     * @returns {Promise}
     * @throws When request parameters are invalid
     */
    sendPreviewRequest(post) {
        if (!post.hasOwnProperty('BID') ||
            !post.hasOwnProperty('token') ||
            !post.hasOwnProperty('thread_title') ||
            !post.hasOwnProperty('message') ||
            post['BID'] <= 0) {
            throw new Error('Invalid request parameters.');
        }

        post['message'] = this.parseFinalMessage(post['message']);

        // Add default values
        post['action'] = 'preview';
        post['preview'] = 'Preview';

        return fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/newthread.php', {
            'method': 'POST',
            'body': GeneralUtility.createFormData(post)
        });
    }


    /**
     * Performs the request for creating a new thread.
     *
     * @param {JSON} post Post values
     * @param {int} post.BID Board
     * @param {string} post.token Token
     * @param {string} post.thread_title Title
     * @param {string} post.message Initial post message
     * @param {string} [post.thread_subtitle] Subtitle
     * @param {int} [post.thread_icon] Icon identifier
     * @param {string} [post.thread_tags] Comma separated tags
     * @param {int} [post.thread_converturls] Enable [URL] conversion
     * @param {int} [post.thread_disablebbcode] Disable BB-Code
     * @param {int} [post.thread_disablesmilies] Disable smilies
     *
     * @returns {Promise}
     * @throws When request parameters are invalid
     */
    sendPostRequest(post) {
        if (!post.hasOwnProperty('BID') ||
            !post.hasOwnProperty('token') ||
            !post.hasOwnProperty('thread_title') ||
            !post.hasOwnProperty('message') ||
            post['BID'] <= 0) {
            throw new Error('Invalid request parameters.');
        }

        post['message'] = this.parseFinalMessage(post['message']);

        // Add default values
        post['submit'] = 'Eintragen';

        return fetch(GeneralUtility.getProtocol() + 'forum.mods.de/bb/newthread.php', {
            'method': 'POST',
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            'body': GeneralUtility.createQueryString(post)
        });
    }
}
