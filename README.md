# pOT Enhancer

Enhances the browsing experience on forum.mods.de. This is a purely experimental fork of the Firefox addon. Consider changing to Firefox.

# Differences between Firefox and Chrome extension

- `./manifest.json`:
    - Remove `browser_specific_settings` block.
- `./src/frontend/frontend.js`:
    - Replace settings-load-promise with callback-variant.
    - Remove `chrome.storage.local.clear();`.
- `./src/frontend/routes/thread-route.js`:
    - Replace `browser.runtime.getURL` with `chrome.runtime.getURL`.
- `./src/frontend/utilities/general-utility.js`:
    - Replace `browser.runtime.getURL` with `chrome.runtime.getURL`.
    - Replace download-method:

````js
/**
 * Pushes the given content as a download to the client.
 *
 * @param {*} content Content to download
 * @param {string} filename Filename for holding the content
 * @param {string} mimeType What type of download
 * @param {function} callback Callback when the download is ready
 *
 * @returns {void}
 *
 * @throws When the browser object is missing
 */
static
download(content, filename, mimeType, callback)
{
    if (chrome === undefined) {
        throw new Error('Cannot download, as "chrome" is undefined.');
    }

    let file = new Blob([content], {'type': mimeType});
    let url = URL.createObjectURL(file);

    let downloading = chrome.downloads.download({
        'url': url,
        'filename': filename
    }, callback);
}
````

- `./src/frontend/components/ediotr/post-editor.js`:
    - Replace JS-token generation `token = window.wrappedJSObject.token_newreply;`:

````js
// TokenID cannot be access directly via wrappedJSObject like in Firefox addon
// Sucky workaround below, but thats chrome-extensions for 'ya
for (let i = 0; i < window.document.scripts.length; i++) {
    let script = window.document.scripts[i].innerText;
    let matches = RegExUtility.find(script, new RegExp('var token_newreply = \'([a-zA-Z0-9]+)\';', 'g'));
    if (matches.length > 0) {
        token = matches[0];
        break;
    }
}

if (!token) {
    throw new Error('Could not get token.');
}
````

- `./src/backend/backend.js`:
    - Replace settings-load-promise with callback-variant.
- `./src/backend/settings/settings.js`:
    - Remove `browser.storage.sync.set` block.
    - Replace `browser.storage.local.set` with `chrome.storage.local.set`.
    - Replace load-method:

````js
/**
 * Loads settings from the storage. Should always be called initially by the app.
 *
 * @param {function} callback Called when the settings were loaded
 *
 * @returns {void}
 */
load(callback)
{
    DebugUtility.log('Load settings.');

    // Get settings from local cache, if possible
    chrome.storage.local.get('settings', (localResults) => {
        DebugUtility.log('Local settings promise.');
        DebugUtility.log(GeneralUtility.clone(localResults));

        if (GeneralUtility.isEmpty(localResults)) {
            DebugUtility.log('Local settings empty.');
            this._settings = this.parse(null);
        } else {
            DebugUtility.log('Local settings present.');
            this._settings = this.parse(localResults.settings);
        }

        callback(this._settings);
    });
}
````

- `./src/backend/components/export-settings/export-settings-view.js`:
    - Replace download-method:

````js
GeneralUtility.download(settings, 'settings-' + DateUtility.timestamp + '.json', 'application/json', downloadId => {
    this.statusContainer.append(GeneralUtility.createAlert('success', 'Settings downloaded.', 5000));
});
````
