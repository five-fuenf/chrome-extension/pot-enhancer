/**
 * Base class for a model.
 *
 * @author FiveFuenf
 */
class AbstractModel {
    /**
     * Initializes the mode.
     */
    constructor() {
    }


    /**
     * Serializes the model into a saveable JSON.
     *
     * @returns {Object}
     */
    serialize() {
    }


    /**
     * Deserializes a serialized JSON into a proper object.
     *
     * @param {Object} serialized Serialized object
     * @returns {Object} Deserialized object
     */
    static deserialize(serialized) {
    }
}
