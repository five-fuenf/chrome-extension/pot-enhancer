/**
 * Initializes the correct module for the current route.
 *
 * @author FiveFuenf
 */
class Router {
    constructor() {
        // Test URL against defined routes
        let homeRoute = new HomeRoute();
        if (homeRoute.isActive()) {
            DebugUtility.log('HomeRoute is active.');
            document.body.classList.add('home');
            Router.activeRoute = homeRoute;
            homeRoute.initialize();
        }

        let boardRoute = new BoardRoute();
        if (boardRoute.isActive()) {
            DebugUtility.log('BoardRoute is active.');
            document.body.classList.add('board');
            Router.activeRoute = boardRoute;
            boardRoute.initialize();
        }

        let threadRoute = new ThreadRoute();
        if (threadRoute.isActive()) {
            DebugUtility.log('ThreadRoute is active.');
            document.body.classList.add('thread');
            Router.activeRoute = threadRoute;
            threadRoute.initialize();
        }

        let createThreadRoute = new CreateThreadRoute();
        if (createThreadRoute.isActive()) {
            DebugUtility.log('CreateThreadRoute is active.');
            document.body.classList.add('create-thread');
            Router.activeRoute = createThreadRoute;
            createThreadRoute.initialize();
        }

        let createThreadRedirectRoute = new CreateThreadRedirectRoute();
        if (createThreadRedirectRoute.isActive()) {
            DebugUtility.log('CreateThreadRedirectRoute is active.');
            Router.activeRoute = createThreadRedirectRoute;
            createThreadRedirectRoute.initialize();
        }

        let createPostRoute = new CreatePostRoute();
        if (createPostRoute.isActive()) {
            DebugUtility.log('CreatePostRoute is active.');
            document.body.classList.add('create-post');
            Router.activeRoute = createPostRoute;
            createPostRoute.initialize();
        }

        let createPostRedirectRoute = new CreatePostRedirectRoute();
        if (createPostRedirectRoute.isActive()) {
            DebugUtility.log('CreatePostRedirectRoute is active.');
            Router.activeRoute = createPostRedirectRoute;
            createPostRedirectRoute.initialize();
        }

        let editPostRoute = new EditPostRoute();
        if (editPostRoute.isActive()) {
            DebugUtility.log('EditPostRoute is active.');
            document.body.classList.add('edit-post');
            Router.activeRoute = editPostRoute;
            editPostRoute.initialize();
        }

        let editPostRedirectRoute = new EditPostRedirectRoute();
        if (editPostRedirectRoute.isActive()) {
            DebugUtility.log('EditPostRedirectRoute is active.');
            Router.activeRoute = editPostRedirectRoute;
            editPostRedirectRoute.initialize();
        }

        let previewPostRoute = new PreviewPostRoute();
        if (previewPostRoute.isActive()) {
            DebugUtility.log('PreviewPostRoute is active.');
            Router.activeRoute = previewPostRoute;
            previewPostRoute.initialize();
        }
    }


    /**
     * Gets the current active route.
     *
     * @returns {Route}
     */
    static get activeRoute() {
        return Router._activeRoute;
    }


    /**
     * Sets the currently active route.
     *
     * @param {Route} activeRoute Active route
     * @returns {void}
     */
    static set activeRoute(activeRoute) {
        Router._activeRoute = activeRoute;
    }
}

// Will contain the currently active route
Router._activeRoute = null;
